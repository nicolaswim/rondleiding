# Introduction

```{image} intro_beeld.png
:alt: catch
:class: bg-primary mb-1
:width: 1080px
:align: center
```

Welcome to the captivating world of Antwerp tours, meticulously curated by renowned Art Historian Serge Landuyt. Dive into a realm where every corner of Antwerp unfolds like a page from history, brought to life through the expert eyes of Serge. Our website is your gateway to exploring Antwerp's rich tapestry of art, architecture, and culture, with tours designed to enlighten, entertain, and inspire.

Serge Landuyt, with his profound knowledge and passion for art history, has crafted a variety of tours that cater to all interests. Whether you're drawn to the grandeur of Renaissance art, the intricate details of Gothic architecture, or the vibrant streets that inspired centuries of artists, our tours offer unparalleled insights into the heart and soul of Antwerp.

Join us as we traverse the cobbled lanes of the city center, unveiling the stories behind iconic landmarks and hidden gems alike. From the majestic Central Station to the serene spaces of Rubenshuis, each tour is an adventure, a chance to see Antwerp through the lens of an art historian. Serge's engaging narratives and deep expertise ensure a journey that's as educational as it is enchanting.

Prepare to immerse yourself in the beauty and brilliance of Antwerp. Whether you're a first-time visitor or a seasoned explorer, our tours promise a fresh perspective on this historic city. Let's embark on a journey of discovery, exploring the places where art and history intersect, guided by the expertise of Serge Landuyt. Welcome aboard!