# tunus

A research project concerning the future of living, modulair compositions and technological innovation. You can download the report with this [link](tunus_tiny_house_project.pdf). Alternatively, you can read about it in the TU Delft Repository following this [link](https://repository.tudelft.nl/islandora/object/uuid%3Ade3a7d8b-f558-4bd7-affb-27e7fedf3b8f).

```{image} DSC_0114.NEF.jpg
:alt: spot
:class: bg-primary mb-1
:width: 500px
:align: center
```

## Introduction

In this project, the future of tiny house communities is researched by an interdisciplinary team of four
bachelor students of the TU Delft from the faculties of Aerospace, Mechanical, Electrical, and Industrial
Design Engineering. This report presents a design of a tiny house as well as the intelligent network the
tiny house is part of.

The research was done over two phases of ten weeks. The first one aimed at analysing existing tiny
house trends and gathering information from a variety of courses to help in this orienting phase, ranging
from architectural design to sustainable energy generation, from waste management to electrical grid
design.

The second phase, the hands­on design of the tiny house and the network is concluded by this
report. All findings and designs can be found in this report, for additional information and elaborated
topics, the appendix is referred to.

We are an interdisciplinary team built up of four friends. This project is an objectification of that friendship. Our history leads us back to the city where we all grew up: Antwerp, Belgium. We are high­school
friends constantly questioning and challenging the world around us, but most importantly, each other.
The COVID­19 pandemic created an opportunity for us to rejoin forces and forge a new challenge, this
project.

Apart from the contents of this project, its essence aims at teaching interdisciplinary cooperation
and management of resources. It teaches not how to colour but how to draw the lines.

Delft, 20 January 2021

Korneel, Onno, Pieter, and Wim