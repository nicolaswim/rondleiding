# ROS-based SpotMicro 
TU Delft - Mechanical Engineering (Cognitive Robotics) Bachelor's Thesis

```{image} spot.jpg
:alt: spot
:class: bg-primary mb-1
:width: 500px
:align: center
```

## Introduction
According to 'How robots change the world' by Oxford Economics, Robots could take over 20 million jobs by 2030. It is no longer a question of if robotics will shape our future, but how much and in what ways. This makes teaching young students about robotics all the more important. 

This is the bachelor thesis of four mechanical engineering students of the 3mE faculty at the Delft Technical University. This bachelor thesis was made under the supervision of MSc. Martin Klomp, ir. Ehsan Hosseini and ir. André van der Kraan of the Cognitive Robotics Department.

This report describes the design project called 'Building a low-cost ROS-based Quadruped SpotMicro Robot'. The Cognitive Robotics project has the aim to design a low-cost robot that uses components of the Mirte robot kit. Mirte is a robot developed for education, that includes hardware and software. Mirte was created with the idea that educational robots should be cheap, open source, focus on more than just software, have as few custom parts as possible and should be useful from primary/elementary school all the way to university. 
The Mirte components that are used in the Spotmicro robot are the OrangePi Zero and the STM32 MINI Controller. The software is a python api for ROS, making it possible to control robots from a python script. 

SpotMicro is an opensource robot which takes design elements from the famous spot robot by Boston Dynamics. There are a number of existing designs for the SpotMicro, of which we chose one. Therefore, the design challenge of this project is mostly in research in servo motor selection and the programing of the robot to make SpotMicro work with the Mirte components and software. 

The parts that were used to build the robot were all taken from the SpotMicroESP32 project of Michael Kubina and can be found here:
https://github.com/michaelkubina/SpotMicroESP32

In the simulations folder, the theoretical models for the predictions of the motor torque requirements can be found. All the forward kinametics for the robot control can be found in the code folder. All the STL's that were designed by us for the Mirte hardware can be found in the parts folder. 

You can download the paper thesis through the following [link](Thesis_Paper.pdf)

## Simulations

In order to predict the minimal required motor torque, multiple
theoretical models were be made. The first model was  a
motion study in Solidworks. Next, a dynamical kinematics
model was be made with the use of Python. From the findings
of the theoretical models, a motor selection was done and
implemented in an actual robot. And lastly, in order to validate
the models, a video analysis of the motion of the robot was
done with the use of Coach 7 and Python.

The SolidWorks motion study modeled the following movements and returned the torques that were deliverd by the motors in the joints. 
The results of this motion study are plotted in the graphs below. 

```{image} StandUp-2.gif
:alt: standup
:class: bg-primary mb-1
:width: 500px
:align: center
```

```{image} Artboard0.png
:alt: standup
:class: bg-primary mb-1
:width: 500px
:align: center
```

The peak in motor torque is 732 N-mm and witnessed in the wrists of the front legs of the robot.  

```{image} RearFirst-2.gif
:alt: rearfirst
:class: bg-primary mb-1
:width: 500px
:align: center
```

```{image} Artboard1.png
:alt: standup
:class: bg-primary mb-1
:width: 500px
:align: center
```


The peak in motor torque is 702 N-mm and witnessed in the wrists of the front legs of the robot.  
From these simulations it became clear that standing up with the rear legs first requires less torque from the motors. 

Next to the motion a study, a dynamical kinematics model was made with the help of a python script. This theoretical model will calculate the required torques based on two components: the force of gravity and the angular momentum of the servo motors. The model simulates the movement of the servomotors: it outputs the velocity and acceleration of the servomotors at their respective position and thus gives an insight in the requirements the motors. See the paper for a more thorough explanation of the model. 
The code of this model can be found in the Code folder. 

```{image} torque-02.png
:alt: torque
:class: bg-primary mb-1
:width: 500px
:align: center
```

After the robot was built, a video analysis of the motion of the robot was made to validate the theoretical models and to find a
conclusive answer to what the real torque is that is required to stand up. The validation is performed on the robot built with
the MG996R servo motors. Coach 7 was used to map out the x and y coordinates of the foot tip, wrist and elbow of the rear and front leg of the robot, a snapshot of this process can be seen below.

```{image} snippet_coach7.jpg
:alt: spot
:class: bg-primary mb-1
:width: 500px
:align: center
```

These coordinates are then imported in a python script that maps out the real angular position, velocity and acceleration of the servomotors. These real coordinates are then used to simulate the movement based on the real movements of the robot, which can be seen in the GIF format here below. It should be noted that because it is still assumed that the robot is symmetric over its longitgitudinal axis, a profile of the robot is taken.

![Coach 7 simulation](animation.gif)