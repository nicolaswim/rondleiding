# 202306 Deadlines

<iframe width="560" height="315" src="https://www.youtube.com/embed/IldjEaUEpR0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

```
wanneer de klokslag klinkt en het uur is verstreken
wanneer de spanning hoog is en het verleden is vergeten
wanneer de tijd nu is en mijn lichaam nog niet genoeg heeft geleden
klik ik op inleveren

want deadlines zijn als een spirituele ervaring
een drugstour van caliber
en eindeloos geklieder

# een deadline heeft een begin en een rust

ik zit en ik sus mezelf
dat de tijd ruimschoots mij te vergeven heeft
dat ontwikkeling kan 
dus ga naar de cinema
schrijf een gedicht
bezoek je tante in het gesticht
plan een vakantie 
herograniseer je kamer
vergeet je werk
maak je leven aangenamer 

# een deadline heeft een midden en een limbo

ik zit gevangen tussen de wal en de dijk
ik zit gevangen tussen de theorie en de praktijk 
zou ik nu dan echt iets moet offeren
om te snappen hoe het werkt
of kan ik teren op het zerk
op de sympathie van mijn collega
op het goeddunken van een docent
kan ik plagiëren op een gekend weerga
ik plan voorzichtig de tijd
om zo net niets te moeten doen
ik wacht tot het einde mij bevrijd


# een deadline heeft een eind en een climax

want eens het ritme versnelt
en de dagen zijn geteld
telt elke seconde dubbel
dan wordt ik maniakaal snel
en dan ontploft mijn bubbel 
waar ik dacht te kunnen zweven 
als de deadline nadert
stopt de tijd heel even
maar in mijn beleving is dat oneindig
want de woorden stromen als waanzinnige dromen 
de code kaapt mijn korrelige blad en mijn deadline wordt nat 
bespat door bloed, zweet maar nooit tranen
mijn deadline moet behaald worden zonder falen
dus bijt ik me door de barre brokken van het bestaan
door de brocante boulderaars en de bevoegde beschonken beleving
bolster ik de bijtende boecht 
die ik zo last-minute weet te produceren

ineens besef ik het concept "planning"
en waarom ik het voortaan zou moeten eren

apocalyptisch postuleer ik een conclusie 
abnormaal snel vind ik de formule tot kernfusie
want ineens is het innerlijk genie in mij gewekt
zolang ik maar de tijd tot de deadline rek

en als dan stil trippelend 
hard kietelend 
de klokslag slaakt 
is het de verplichting die mij staakt
want ik moet inleveren op een knop
ik moet nu toegeven aan de tijd
anders word ik zot


# een deadline heeft een gloed en een leegte

mijn hart klopt duizend keer in mijn keel 
mijn bloeddruk verlaagt
en mijn geweten word rustig weer heel 
het plakt en het klieft 
de gedachtes worden mij weer lief
dat de hel is gebeurd
en de tijd is verstreken

mijn lichaam trekt leeg 
en een zog van tijd en moeite creëert zich rondom me
tussen de lijnen van het bestaan vormen zich bellen van zwart 
tijd is een vak apart 


dus voortaan als ik de datum zie staan 
zal ik plannen en blijven doorgaan
zal ik gehoorzamen aan de stemmen
en zal ik mijn nevenactiviteiten mennen

maar dan 
diep in de verte
lonkt daar weer de gezelligheid
en word ik verstoord
ik pak een pilsje
en gooi alles weer overboord
```