### 202001 Twee wegen

```{image} wegen.png
:alt: wegen
:class: bg-primary mb-1
:width: 500px
:align: center
```

```none
Mijn leven is opgesplitst in twee wegen 

Ik kan zwichten ik kan beven 
Ik kan liegen en bedriegen
Verloochenen voor mijn doel
Mijn keuzes?
koud kil en koel

Mijn hart
Rood kloppend 
van 	Verdriet
		Liefde
		Gemis
		Pijn
		Gedis
		Zijn

Ik ben het beu om 
van opstaan tot val
van lopen tot kruipen
van trots tot druipen
te leven

Van net wakker worden tot 
Ah shit
Ik heb weer iets tegen mijn kop gekregen

Ik kan onmogelijk 
moeiteloos rusten 
zorgeloos sussen 
ongepijnigd kijken
Terwijl wat ik ben is aan het bezwijken

Hoe kan ik mijn gevoelens door de vingers zien
Hoe kan ik mijn hersens spoelen tot ik dingen zie 
Hoe kan ik nu helder kiezen
Als het uiteindelijke doel toch is: verliezen

Ik zit in de knoop
Mijn externe factoren?
helpen niet echt een hoop

Ik zit diep in de existentiële shit
Zonder te weten waar mijn oplossing zit

Wie heb ik echt rond mij
Mijn moeder. Mijn broeder.
Mijn vriendin. Mijn ego. 
Mijn zus. Mijn vader.
Mijn brief. Mijn zegel.
Mijn mobiel. Mijn lader.

Mijn problemen zijn er nog steeds 
Die twee paden
Die zijn niet weggegaan
Die zijn niet magisch verdwenen
Behoorden niet opeens tot het verleden

Nee nee, vergis u niet
Die tweesprong is een dagelijkse realiteit
Een kut waar ik elk uur in kijk
Twee gespreide benen zonder genade
Een femma fatele
het einde van mijn dagen

Het enige dat ik wil is die twee pootjes 
samenduwen
samenkneden 
Doen smelten
Tot een rechte autostrade
Een weg die zo mooi is uitgestippeld 
Dat ingenieurs er van tranen
Een hermetische toekomst
Omgeven door ongebroede stront

Maar 

Dat kan niet 
leven
Zonder twijfel, geen bestaan
Zonder keuze, geen winst
De kunst is
leren laten gaan
En winnen, zonder hints
```